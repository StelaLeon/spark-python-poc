    
How to run this? Please follow the Makefile: the targets are self explanatory. 

    build: builds the docker imaged used by the env/docker compose, bakes the code and the data inside the image
    build-env: spins up a docker environment with 1  server and 1 worker
    clean-env: tears down the environment and removes the docker-networks
    submit-job: submits the job baked inside the docker images

Once we build the docker image running the main and the worker, we already backed the apps inside and we mapped the dick-volumes, the problem here 
appears when we need to update the code as it needs to be redeployed. Not ideal at all, we should bake the code inside of a submit-docker container and just update that one.

We should use serverless and run away from hdfs as it's slow.

What can we improve? 

Automatic formatting of the code: python-@TODO
Verify the pattern of _corrupt_record and check the rows to respect the rule we observed.

In order to extend this to a big volume of data we can speed things up on various level:
   a. cache the DFs that we processed, we will use them again for processing and therefore we should cache them. 
   b. Use Datasets in scala apps so we can enjoy the stability of type safety


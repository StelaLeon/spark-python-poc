import pyspark
from pyspark.sql import SparkSession
from pyspark.sql.functions import col,date_format
from pyspark.sql.types import StructType, StructField, StringType, BooleanType, ShortType, IntegerType
from pyspark.sql.functions import col, from_json, to_timestamp, when, unix_timestamp, count, min
from pyspark.sql.types import *

def init_spark():
    spark = SparkSession.builder \
        .config("spark.executor.pyspark.memory", "512m") \
        .config("spark.python.worker.memory", "1G") \
        .appName("spond-app") \
        .getOrCreate()
    sc = spark.sparkContext
    return spark,sc

def readProfile(file, spark):
    return spark.read.option("header", True).parquet(file)

def readFile(file, format, spark):
    return spark.read.format(format).load(file)

def cleanGroups(groups):
    schema = StructType([ \
        StructField("group_name", StringType(), True),
        StructField("country_code", StringType(), True),
        StructField("is_admin", BooleanType(), True),
        StructField("profile_id", IntegerType(), True),
    ])
    dfJSONToFilter = groups.na.drop(subset= ["_corrupt_record"]) \
        .withColumn("parsed_record",from_json(col("_corrupt_record"),schema) ) \
        .select(
        col("country_code").alias("cc"),
        col("group_name").alias("gn"),
        col("is_admin").alias("ia"),
        col("profile_id").alias("pi"),
        col("parsed_record.profile_id").alias("parsed_profile_id")
    ).na.drop(subset=["parsed_profile_id"])
    cleanedGroups = groups \
        .withColumnRenamed('profile_id', 'parsed_profile_id') \
        .join(dfJSONToFilter, ["parsed_profile_id"],"left_anti") \
        .na.drop(subset=["parsed_profile_id"]) \
        .withColumnRenamed("parsed_profile_id","profile_id") \
        .select("profile_id", "country_code", "group_name", "is_admin")
    return cleanedGroups

def cleanProfilesCreatedAt(profilesCreatedAt):
    return profilesCreatedAt.na.drop(subset=["externalid"])

def getClubAdmins(cleanedGroups, profilesCreatedAt, unsubscribeDF):
    admins_us_run_tennis_club = cleanedGroups \
        .filter(col("country_code").like('USA')) \
        .filter(col("group_name").like('%Tennis%Club%') | col("group_name").like('%Running%Club%')) \
        .filter(col("is_admin") == True)

    subscribed_profiles = profilesCreatedAt \
        .join(  unsubscribeDF.select(col("_c0").alias('email')),['email'], "left_anti")

    us_clubs_subscribed = admins_us_run_tennis_club.join(subscribed_profiles, ['profile_id'], 'inner')
    return us_clubs_subscribed.select('email', 'is_admin', 'group_name', 'country_code', 'externalid')

"""
Spark seems to accept timestamp in seconds rather than millis therefore I am casting millis into secs in the query
This utility will cast a column from seconds: to timestamp
"""
def castLongColumnsToTs(df, col1, col2):
    return df \
        .withColumn(col1, (col(col1) / 1000).cast('timestamp')) \
        .withColumn(col2, (col(col2) / 1000).cast('timestamp'))

def verifyProfilesData(profileHistoryDF, newProfilesDF):
    # is the profile id unique?
    assert(
            profileHistoryDF.select(col('profile_id')).count() == profileHistoryDF.select(col('profile_id')).distinct().count()
    )

    assert(
            newProfilesDF.select(col('profile_id')).count() == newProfilesDF.select(col('profile_id')).distinct().count()
    )

    print("There are ten records that were removed between the times t=0 and t=1. It's crucial to keep these records in the consolidated dataset.")
    #let's verify this
    oldProfiles = profileHistoryDF.select(col('profile_id')).distinct().toDF('profile_id')
    newProfiles = newProfilesDF.select(col('profile_id')).distinct().toDF('profile_id')

    removedProfilesNo = oldProfiles.exceptAll(newProfiles)
    assert(removedProfilesNo.count() == 10)

    print("Ten records have experienced changes in their email_address property between time t=0 and t=1.")
    #let's verify this

    email_address_changes = getEmailChanges(profileHistoryDF, newProfilesDF)
    assert(email_address_changes.count()==10)

def getEmailChanges(profileHistoryDF, newProfilesDF):
    return profileHistoryDF \
        .withColumnRenamed('email', 'past_email') \
        .join(newProfilesDF, ['profile_id'], 'inner') \
        .filter(col('past_email') != col('email'))

def mergeProfileData(profileHistoryDF, newProfilesDF):
    merged_profile_history_raw_df = profileHistoryDF \
        .withColumnRenamed('external_id', 'past_external_id') \
        .withColumnRenamed('gender', 'past_gender') \
        .withColumnRenamed('first_name', 'past_first_name') \
        .withColumnRenamed('last_name', 'past_last_name') \
        .join(newProfilesDF, ['profile_id', 'email'], 'outer') \

    updated_ts_profile_history_df = merged_profile_history_raw_df \
        .withColumn('valid_to', when(col('external_id').isNull(), unix_timestamp() * 1000).otherwise(4102358400000)) \
        .withColumn('valid_from', when(col('past_external_id').isNull(), unix_timestamp() * 1000).otherwise(when(col('valid_from').isNull(), unix_timestamp() * 1000).otherwise(col('valid_from')))) \
        .withColumn('first_name', when(col('first_name').isNull(), col('past_first_name')).otherwise(col('first_name'))) \
        .withColumn('last_name', when(col('first_name').isNull(), col('past_last_name')).otherwise(col('last_name'))) \
        .withColumn('gender', when(col('gender').isNull(), col('past_gender')).otherwise(col('gender'))) \
        .withColumn('external_id', when(col('external_id').isNull(), col('past_external_id')).otherwise(col('external_id'))) \

    return updated_ts_profile_history_df

def verifyMergedProfiles(updated_ts_profile_history_df, email_address_changes, removedProfilesNo):
    removed_entries = updated_ts_profile_history_df \
        .groupBy('profile_id') \
        .agg(
        count("email").alias('count'), \
        min(col('valid_to')).alias('temp_valid_to')) \
        .select('profile_id') \
        .where(col('count') == 1) \
        .filter(col('temp_valid_to') <= (unix_timestamp() * 1000))

    updated_entries = updated_ts_profile_history_df.groupBy('profile_id').count().select('profile_id').where(col('count') == 2)
    assert(removed_entries.select('profile_id').exceptAll(removedProfilesNo).isEmpty())
    assert(email_address_changes.select('profile_id').exceptAll(updated_entries).isEmpty())


def getRemovedProfiles(profileHistoryDF, newProfilesDF):
    oldProfiles = profileHistoryDF.select(col('profile_id')).distinct().toDF('profile_id')
    newProfiles = newProfilesDF.select(col('profile_id')).distinct().toDF('profile_id')
    return oldProfiles.exceptAll(newProfiles)

def main():
    spark,sc = init_spark()
    print(sc.getConf().getAll())

    print("Starting processing question Q2.1")

    cleanedGroups = cleanGroups(readProfile("datalake/group/group.parquet", spark))
    profilesCreatedAt = cleanProfilesCreatedAt(readFile("datalake/profile/created_at=2023-01-05/profile.json", "json", spark))
    unsubscribeDF = readFile("datalake/unsubscribe/unsubscribe.csv", "csv", spark)

    club_admins = getClubAdmins(cleanedGroups, profilesCreatedAt, unsubscribeDF)

    print("Starting processing question Q2.2")

    newProfilesDF = readFile("datalake/profiles_history/new_profiles.json", "json", spark)
    profileHistoryDF = readFile("datalake/profiles_history/profiles_history.json", "json", spark)

    verifyProfilesData(profileHistoryDF, newProfilesDF)

    updated_ts_profile_history_df = mergeProfileData(profileHistoryDF, newProfilesDF)

    email_address_changes = getEmailChanges(profileHistoryDF, newProfilesDF)
    removedProfilesNo = getRemovedProfiles(profileHistoryDF, newProfilesDF)
    verifyMergedProfiles(updated_ts_profile_history_df, email_address_changes, removedProfilesNo)


    merged_profile_history_df = castLongColumnsToTs(updated_ts_profile_history_df, 'valid_from', 'valid_to') \
        .drop('past_external_id') \
        .drop('past_first_name') \
        .drop('past_last_name') \
        .drop('past_gender')

    print("Let's write on disk. ")

    club_admins.coalesce(1).write.option("header",True).csv("datalake/output/club_admins.csv")
    merged_profile_history_df.coalesce(1).write.option("header",True).csv("datalake/output/merged_profile_history_df.csv")

if __name__ == '__main__':
    main()

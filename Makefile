.PHONY: build
build:
	docker build -t spark-cluster .

.PHONY: build-env
build-env:
	docker-compose up

.PHONY: clean-env
clean-env:
	docker-compose down

.PHONY: submit-job
submit-job:
	docker exec -it $(shell docker ps -aqf "name=spark-main") /opt/spark/bin/spark-submit  /opt/spark-apps/main.py
